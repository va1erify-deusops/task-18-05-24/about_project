# Моя четвертая задача от [Deusops](https://deusops.com/)

***

# Условия и ссылки
<details>
<summary>Light:</summary>

1. Разверните виртуальную машину для экспериментов. Можно использовать любые облака, хостинги и способы создания, в том числе и локально на Vagrant
2. Разберитесь, как запускать приложение локально
3. Создайте Dockerfile для приложения. Убедитесь, что все зависимости и настройки включены. Соберите Docker-образ приложения локально и убедитесь, что он успешно запускается
4. Создайте учетную запись на Docker Hub и выложите ваш Docker-образ
5. Создайте репозиторий в gitlab для приложение и настройте CI/CD пайплайн для автоматической сборки Docker-образов
6. Добавьте шаг с публикацией образа в gitlab-registry после сборки и шаг с “релизом” образа в docker-hub

</details>

***

<details>
<summary>Normal:</summary>

1. Настройте базовый Kubernetes кластер. Можно использовать Minikube для локальной разработки или облачный провайдер
2. Установите Helm на свою локальную машину или на сервер, с которого вы будете управлять Kubernetes кластерами. Установите ingress-контроллер в Kubernetes-кластер используя Helm
3. Создайте Helm-чарт для приложения, включив и описав все необходимые ресурсы Kubernetes (Deployment, Service, ConfigMap, Secret), находящиеся в директории "templates/"
4. Добавьте в Helm-чарт ingress.yaml, в котором опишите конфигурацию для доступа к вашему приложению извне
5. Заполните файл `values.yaml` переменными для конфигурирования приложения (например, имя образа Docker, количество реплик и т.д.)
6. Создайте отдельный репозиторий для вашего helm-чарта. Используйте Helm для деплоя вашего приложения в Kubernetes кластер
7. Добавьте в CI/CD-пайплайн приложения шаг для деплоя через Helm. helm install должен получать чарт в виде tar.gz через api Gitlab

</details>

***

<details>
<summary>Heroic:</summary>

1. Настройте горизонтальное автоскалирование (HPA) для вашего приложения в Kubernetes
2. Обновите Helm-чарт для поддержания версионирования и откатов развертываний
3. Используйте Helm для создания и управления несколькими окружениями (dev, staging, prod). Проработайте cicd-пайплайн для развертвания на разные окружения. Учтите разные values для разных окружений. Придумайте механизм для доступа к разным кластерам и управления разными values в рамках одного репозитория
4. Разберитесь с работой helm-package-registry в Gitlab и напишите cicd-пайплайн для helm-чарта с автоматической публикацией чарта

</details>

***

<details>
<summary>Mythic:</summary>

1. Доработайте ваши пайплайны так, чтобы в них был функционал отката деплоя. Разберитесь с возможностями environments в gitlab ci
2. Установите Promethes и Grafana и добавьте работу с ними в ваш helm-чарт для приложения
3. Опишите документацию по вашему проекту и правилам работы с ним и с системой деплоя

</details>

***

<details>
<summary>Links:</summary>

- [Как поднимать виртуальные машины в Vagrant](https://gitlab.com/deusops/lessons/documentation/vagrant)
- [Создание виртуальных машин в Яндекс Клауд ](https://cloud.yandex.ru/ru/docs/tutorials/routing/web-service#create-vms)
- [Подборка информации по Docker](https://gitlab.com/deusops/lessons/documentation/docker)
- [Подборка информации по Gitlab CI](https://gitlab.com/deusops/lessons/documentation/gitlab-ci)
- [Гайды по Kubernetes](https://gitlab.com/deusops/documentation/kubernetes)
- [Документация по Helm](https://gitlab.com/deusops/documentation/helm)
- [Шпаргалка по коммандам Helm ](https://gitlab.com/deusops/documentation/helm/-/blob/main/cheatsheet/README.md)
- [Приложение (исходники)](https://gitfront.io/r/deusops/Fsjok1dx89xG/flask-project-01/)

</details>

***
# Описание реализации
1. **Создание/хранение инфраструктуры**:
    - По заданию 3 контура - [dev](https://gitlab.com/va1erify-deusops/task-18-05-24/infrastructure/state/dev), [staging](https://gitlab.com/va1erify-deusops/task-18-05-24/infrastructure/state/staging) и [prod](https://gitlab.com/va1erify-deusops/task-18-05-24/infrastructure/state/prod).
    - Развертывание происходит в Yandex Cloud с помощью Terraform.
    - Terraform backend хранится в Gitlab.
    - Terraform манифесты для каждого контура хранятся в отдельном репозитории.
    - Переменные и секреты вынесены в закрытый репозиторий.
    - [Gitlab CI для развертывания инфраструктуры](https://gitlab.com/va1erify-deusops/task-18-05-24/devops/gitlabci-flows/gitlabci-flow-infrastructure) также хранится в отдельном репозитории и версионируется

2. **Настройка кластера**:
    - В K8s кластер с помощью [Gitlab CI](https://gitlab.com/va1erify-deusops/task-18-05-24/infrastructure/provision/k8s-provision) через helm происходит установка ingress-nginx-controller, grafana, prometheus, cert-manager.
    - Values для helm чартов вынесены в закрытий репозиторий.

3. **Приложение и деплой**
    - [Приложение](https://gitlab.com/va1erify-deusops/task-18-05-24/development/flask-project/app) вынесено в отдельный репозиторий.
    - [GitlabCI](https://gitlab.com/va1erify-deusops/task-18-05-24/devops/gitlabci-flows/gitlabci-flow-app) для сборки и деплоя приложения вынесен в отдельный репозиторий.
    - [Dockerfile](https://gitlab.com/va1erify-deusops/task-18-05-24/devops/docker-images/flask_project) вынесен в отдельный репозиторий.
    - [Helm chart](https://gitlab.com/va1erify-deusops/task-18-05-24/devops/helm-charts/flask-project) вынесен в отдельный репозиторий.
    - Values для приложения вынесены в закрытый репозиторий

***
## Используемые репозитории
- [Gitlab CI для сборки docker образа](https://gitlab.com/va1erify-gitlab-ci/docker-build)
- [Gitlab CI для деплоя инфры через terraform и сохранения backend в Terraform states](https://gitlab.com/va1erify-gitlab-ci/gitlab-terraform-ci)
- [Gitlab CI для package и install helm чарта](https://gitlab.com/va1erify-gitlab-ci/helm)
- [Docker образ с Helm](https://gitlab.com/va1erify-docker-images/helm)
- [Docker образ с Terraform](https://gitlab.com/va1erify-docker-images/terraform)
- [Docker образ с Helm, Kubectl и YandexCLI](https://gitlab.com/va1erify-docker-images/yandexcli-kubectl-helm)
- [Helm чарт с Cert-manager](https://gitlab.com/va1erify-helm-charts/cert-manager-helm-chart)
- [Helm чарт с ClusterIssuer](https://gitlab.com/va1erify-helm-charts/cluster-issuer-helm-chart)
- [Helm чарт с Ingress-nginx controller](https://gitlab.com/va1erify-helm-charts/ingress-nginx-controller-helm-chart)
- [Helm чарт с Grafana, Prometheus и AlertManager](https://gitlab.com/va1erify-helm-charts/prometheus-community-kube-prometheus-stack-helm-chart)
- [Helm чарт с Ingress для Grafana, Prometheus](https://gitlab.com/va1erify-helm-charts/prometheus-grafana-ingress-helm-chart)
- [Terraform модули для Yandex Cloud](https://gitlab.com/va1erify-terraform/yandex-modules/modules)

***
## Используемые инструменты и технологии
- Terraform
- Yandex Cloud
- Kubernetes
- Helm
- Bash
- Python
- Gitlab
- Docker
- Docker Hub
- Prometheus
- Grafana
- LetsEncrypt
- Nginx-ingress controller
- Cert-manager


***
## Скриншоты
![all-project.png](images/all-project.png)
***
![k8s-provision-pipeline.png](images/k8s-provision-pipeline.png)
***
![app-pipeline.png](images/app-pipeline.png)
***
![grafana-dashboards.png](images/grafana-dashboards.png)
***
![grafana-cluster-dashboard.png](images/grafana-cluster-dashboard.png)
***
![grafana-core-dns-dashboard.png](images/grafana-core-dns-dashboard.png)

***
![hpa1.png](images/hpa1.png)
***
![hpa3.png](images/hpa3.png)

***
![endpoing-dev.png](images/endpoing-dev.png)
***
![endpoing-staging.png](images/endpoing-staging.png)
***
![zndpoint-prod.png](images/endpoint-prod.png)